package pl.edu.pwr.course.model;

import pl.edu.pwr.student.model.Student;

import java.util.List;

public class CourseEdition {

    String title;
    String description;
    private final String code;
    List<Student> enrolled;


    public CourseEdition(String title, String description, String code, List<Student> enrolled) {
        this.title = title;
        this.description = description;
        this.code = code;
        this.enrolled = enrolled;
    }

    public void addStudent(Student student) {
        enrolled.add(student);
    }

    public String getCode() {
        return this.code;
    }

    public List<Student> getEnrolled() {
        return enrolled;
    }

    @Override
    public String toString() {
        return "CourseEdition{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", code='" + code + '\'' +
                ", enrolled=" + enrolled +
                '}';
    }
}
