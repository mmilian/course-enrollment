package pl.edu.pwr.course.repository;

import pl.edu.pwr.course.model.CourseEdition;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


public class InMemoryCourseRepository implements CourseRepository {

    private static List<CourseEdition> COURSES_DB = new ArrayList<>();

    @Override
    public List<CourseEdition> findStudentCourses(final String studentId) {
        return COURSES_DB.stream().filter(courseEdition -> courseEdition.getEnrolled().stream().anyMatch(
                student -> student.getId().equals(studentId)))
                .collect(Collectors.toList());
    }

    @Override
    public List<CourseEdition> findAll() {
        return COURSES_DB.stream().collect(Collectors.toList());
    }

    @Override
    public Optional<CourseEdition> findCourseByCode(final String code) {
        return COURSES_DB.stream().filter(courseEdition -> courseEdition.getCode().equals(code)).findFirst();
    }


    @Override
    public void add(CourseEdition courseEdition) {
        COURSES_DB.add(courseEdition);
    }
}