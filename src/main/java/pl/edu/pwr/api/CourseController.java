package pl.edu.pwr.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.pwr.course.model.CourseEdition;
import pl.edu.pwr.course.model.Greeting;
import pl.edu.pwr.course.service.CourseService;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class CourseController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();
    private final CourseService courseService;

    CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping("/greeting")
    public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        System.out.println("Greeting!!!");
        return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }

    @GetMapping("/courses")
    public List<CourseEdition> courses() {
        return courseService.printAllCourses();
    }

}
