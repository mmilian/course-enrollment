package pl.edu.pwr.student.repository;

import pl.edu.pwr.student.model.Student;

import java.util.Optional;

public interface StudentRepository {

    void add(Student student);

    Optional<Student> findById(String studentId);
}
