package pl.edu.pwr;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import pl.edu.pwr.course.model.CourseEdition;
import pl.edu.pwr.course.service.CourseService;
import pl.edu.pwr.student.repository.InMemoryStudentRepository;
import pl.edu.pwr.student.repository.StudentRepository;
import pl.edu.pwr.student.service.StudentService;

import java.util.Arrays;
import java.util.function.Consumer;

@Component("Super-App-Runner")
public class AppRunner implements CommandLineRunner, ApplicationContextAware {

    final private CourseService courseService;
    final private StudentService studentService;
    final private StudentRepository studentRepository;
    private ApplicationContext ctx;

    @Autowired
    public AppRunner(CourseService courseService,
                     StudentService studentService,
                     InMemoryStudentRepository studentRepository) {
        this.courseService = courseService;
        this.studentService = studentService;
        this.studentRepository = studentRepository;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.ctx = applicationContext;
    }

    @Override
    public void run(String... args)  {
        System.out.println("# Beans: " + ctx.getBeanDefinitionCount());
        String[] names = ctx.getBeanDefinitionNames();
        Arrays.sort(names);
        Arrays.asList(names).forEach(System.out::println);

        courseService.addCourse("Spring Intro", "Something about spring", "001");
        courseService.addCourse("Spring Advance", "Deep dive", "002");
        studentService.addStudent("001", "John Down");
        courseService.enrollStudent("001", "001");
        System.out.println(String.join("\n",
                studentRepository.findById("001").get().toString(),
                "Courses:",
                courseService.printCoursesForStudent("001").toString()));
        System.out.println("List all courses:");
        courseService.printAllCourses().forEach(courseEdition -> System.out.println(courseEdition));
    }


}
