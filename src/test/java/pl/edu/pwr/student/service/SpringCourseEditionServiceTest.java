package pl.edu.pwr.student.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pl.edu.pwr.Application;
import pl.edu.pwr.course.repository.CourseRepository;
import pl.edu.pwr.course.service.CourseService;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = Application.class)
class SpringCourseEditionServiceTest {

    @Autowired
    private CourseService courseService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private CourseRepository courseRepository;


    @Test
    void findStudentCourses() {
        courseService.addCourse("spring intro", "something about spring", "001");
        studentService.addStudent("0001", "John");
        courseService.enrollStudent("0001", "001");
        assertEquals(1,
                courseRepository.findStudentCourses("0001").size());
    }

}