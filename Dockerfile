FROM gradle:5.2.1-jdk8-alpine AS build

USER root
COPY src/ /course-enrollment/src/
COPY build.gradle /course-enrollment/
WORKDIR /course-enrollment
RUN gradle bootJar

FROM openjdk:8u191-jdk-alpine3.9
COPY --from=build /course-enrollment/build/libs/* /course-enrollment/
WORKDIR /course-enrollment
CMD java -jar course-enrollment.jar