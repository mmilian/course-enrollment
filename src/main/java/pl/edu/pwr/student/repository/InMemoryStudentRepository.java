package pl.edu.pwr.student.repository;

import org.springframework.stereotype.Component;
import pl.edu.pwr.student.model.Student;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class InMemoryStudentRepository implements StudentRepository {

    private final List<Student> USERS_DB = new ArrayList<>();

    private Map<String, Student> studentByNameIndex;
    private Map<String, Student> studentByIdIndex;

    @PostConstruct
    private void updateCache() {
        studentByNameIndex = indexBy(USERS_DB, Student::getName);
        studentByIdIndex = indexBy(USERS_DB, Student::getId);
        System.out.println("I did my post construct!!!");
    }

    private static Map<String, Student> indexBy(List<Student> students, Function<Student, String> by) {
        return students.stream().collect(Collectors.toMap(by,
                Function.identity()));
    }

    public void add(Student student) {
        USERS_DB.add(student);
        updateCache();
    }

    @Override
    public Optional<Student> findById(String studentId) {
        return Optional.ofNullable(studentByIdIndex.get(studentId));
    }
}
