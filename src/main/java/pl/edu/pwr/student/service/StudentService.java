package pl.edu.pwr.student.service;

import org.springframework.stereotype.Component;
import pl.edu.pwr.student.model.Student;
import pl.edu.pwr.student.repository.StudentRepository;

@Component
public class StudentService {

    private final StudentRepository studentRepository;

    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public void addStudent(String studentId, String name) {
        studentRepository.add(new Student(studentId, name));
    }

}
