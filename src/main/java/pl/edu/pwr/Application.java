package pl.edu.pwr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import pl.edu.pwr.course.repository.CourseRepository;
import pl.edu.pwr.course.repository.InMemoryCourseRepository;


@SpringBootApplication
public class Application {


    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }


    @Bean
    public CourseRepository createCoursesRepository() {
        return new InMemoryCourseRepository();
    }

}
