package pl.edu.pwr.student.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.edu.pwr.course.repository.CourseRepository;
import pl.edu.pwr.course.repository.InMemoryCourseRepository;
import pl.edu.pwr.course.service.CourseService;
import pl.edu.pwr.student.repository.InMemoryStudentRepository;
import pl.edu.pwr.student.repository.StudentRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CourseEditionServiceTest {

    private CourseService courseService;
    private CourseRepository courseRepository;
    private StudentRepository studentRepository;
    private StudentService studentService;


    @BeforeEach
    public void setUp() {
        this.courseRepository = new InMemoryCourseRepository();
        this.studentRepository = new InMemoryStudentRepository();
        this.courseService = new CourseService(courseRepository, studentRepository);
        this.studentService = new StudentService(studentRepository);
    }


    @Test
    void findStudentCourses() {
        courseService.addCourse("spring intro", "something about spring", "001");
        studentService.addStudent("0001", "John");
        courseService.enrollStudent("0001", "001");
        assertEquals(1,
                courseRepository.findStudentCourses("0001").size());
    }
}