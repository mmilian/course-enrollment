package pl.edu.pwr.course.service;

import org.springframework.stereotype.Component;
import pl.edu.pwr.course.repository.CourseRepository;
import pl.edu.pwr.course.model.CourseEdition;
import pl.edu.pwr.student.model.Student;
import pl.edu.pwr.student.repository.StudentRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class CourseService {

    private final CourseRepository courseRepository;
    private final StudentRepository studentRepository;

    /**
     * The database simulator.
     */

    public CourseService(CourseRepository courseRepository, StudentRepository studentRepository) {
        this.courseRepository = courseRepository;
        this.studentRepository = studentRepository;
    }

    public boolean enrollStudent(final String studentId, final String courseCode) {
        Optional<Student> student = studentRepository.findById(studentId);
        Optional<CourseEdition> course = courseRepository.findCourseByCode(courseCode);
        if (student.isPresent() && course.isPresent() && !isEnrolled(student.get(), course.get())) {
            course.get().addStudent(student.get());
            return true;
        }
        return false;
    }

    public Optional<CourseEdition> findByName(final String courseCode) {
        return courseRepository.findCourseByCode(courseCode);
    }

    public void addCourse(String title, String description, String code) {
        courseRepository.add(new CourseEdition(title, description, code, new ArrayList<>()));
    }

    private boolean isEnrolled(Student student, CourseEdition course) {
        return courseRepository.findStudentCourses(student.getId()).contains(course) ? true : false;
    }

    public List<CourseEdition> printCoursesForStudent(String studentId) {
        return courseRepository.findStudentCourses(studentId);
    }

    public List<CourseEdition> printAllCourses() {
        return courseRepository.findAll();
    }
}
