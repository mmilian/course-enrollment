package pl.edu.pwr.course.repository;

import pl.edu.pwr.course.model.CourseEdition;

import java.util.List;
import java.util.Optional;

public interface CourseRepository {

    List<CourseEdition> findStudentCourses(String studentId);

    List<CourseEdition> findAll();

    Optional<CourseEdition> findCourseByCode(String code);

    void add(CourseEdition courseEdition);
}
